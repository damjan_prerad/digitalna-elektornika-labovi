-- Copyright (C) 2016  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Intel and sold by Intel or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "03/20/2021 17:33:31"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          decade_counter
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY decade_counter_vhd_vec_tst IS
END decade_counter_vhd_vec_tst;
ARCHITECTURE decade_counter_arch OF decade_counter_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL clear : STD_LOGIC;
SIGNAL clock : STD_LOGIC;
SIGNAL enable : STD_LOGIC;
SIGNAL output : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL parallel_in : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL write_number : STD_LOGIC;
COMPONENT decade_counter
	PORT (
	clear : IN STD_LOGIC;
	clock : IN STD_LOGIC;
	enable : IN STD_LOGIC;
	output : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
	parallel_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
	write_number : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : decade_counter
	PORT MAP (
-- list connections between master ports and signals
	clear => clear,
	clock => clock,
	enable => enable,
	output => output,
	parallel_in => parallel_in,
	write_number => write_number
	);

-- clear
t_prcs_clear: PROCESS
BEGIN
	clear <= '1';
WAIT;
END PROCESS t_prcs_clear;

-- clock
t_prcs_clock: PROCESS
BEGIN
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
	WAIT FOR 10000 ps;
	clock <= '1';
	WAIT FOR 10000 ps;
	clock <= '0';
WAIT;
END PROCESS t_prcs_clock;

-- enable
t_prcs_enable: PROCESS
BEGIN
	enable <= '0';
	WAIT FOR 250000 ps;
	enable <= '1';
	WAIT FOR 10000 ps;
	enable <= '0';
WAIT;
END PROCESS t_prcs_enable;

-- write_number
t_prcs_write_number: PROCESS
BEGIN
	write_number <= '1';
WAIT;
END PROCESS t_prcs_write_number;
-- parallel_in[3]
t_prcs_parallel_in_3: PROCESS
BEGIN
	parallel_in(3) <= '0';
WAIT;
END PROCESS t_prcs_parallel_in_3;
-- parallel_in[2]
t_prcs_parallel_in_2: PROCESS
BEGIN
	parallel_in(2) <= '0';
WAIT;
END PROCESS t_prcs_parallel_in_2;
-- parallel_in[1]
t_prcs_parallel_in_1: PROCESS
BEGIN
	parallel_in(1) <= '0';
WAIT;
END PROCESS t_prcs_parallel_in_1;
-- parallel_in[0]
t_prcs_parallel_in_0: PROCESS
BEGIN
	parallel_in(0) <= '0';
WAIT;
END PROCESS t_prcs_parallel_in_0;
END decade_counter_arch;
