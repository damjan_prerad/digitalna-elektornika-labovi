-- Copyright (C) 2016  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Intel and sold by Intel or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 16.1.0 Build 196 10/24/2016 SJ Lite Edition"

-- DATE "03/20/2021 17:33:33"

-- 
-- Device: Altera 5CGXFC7C7F23C8 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	decade_counter IS
    PORT (
	parallel_in : IN std_logic_vector(3 DOWNTO 0);
	enable : IN std_logic;
	clear : IN std_logic;
	clock : IN std_logic;
	write_number : IN std_logic;
	output : OUT std_logic_vector(3 DOWNTO 0)
	);
END decade_counter;

ARCHITECTURE structure OF decade_counter IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_parallel_in : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_enable : std_logic;
SIGNAL ww_clear : std_logic;
SIGNAL ww_clock : std_logic;
SIGNAL ww_write_number : std_logic;
SIGNAL ww_output : std_logic_vector(3 DOWNTO 0);
SIGNAL \output[0]~output_o\ : std_logic;
SIGNAL \output[1]~output_o\ : std_logic;
SIGNAL \output[2]~output_o\ : std_logic;
SIGNAL \output[3]~output_o\ : std_logic;
SIGNAL \clock~input_o\ : std_logic;
SIGNAL \write_number~input_o\ : std_logic;
SIGNAL \parallel_in[3]~input_o\ : std_logic;
SIGNAL \state~4_combout\ : std_logic;
SIGNAL \enable~input_o\ : std_logic;
SIGNAL \clear~input_o\ : std_logic;
SIGNAL \state[3]~1_combout\ : std_logic;
SIGNAL \parallel_in[2]~input_o\ : std_logic;
SIGNAL \state~3_combout\ : std_logic;
SIGNAL \parallel_in[1]~input_o\ : std_logic;
SIGNAL \state~2_combout\ : std_logic;
SIGNAL \parallel_in[0]~input_o\ : std_logic;
SIGNAL \state~0_combout\ : std_logic;
SIGNAL \output[0]$latch~combout\ : std_logic;
SIGNAL \output[1]$latch~combout\ : std_logic;
SIGNAL \output[2]$latch~combout\ : std_logic;
SIGNAL \output[3]$latch~combout\ : std_logic;
SIGNAL state : std_logic_vector(3 DOWNTO 0);
SIGNAL \ALT_INV_state[3]~1_combout\ : std_logic;
SIGNAL ALT_INV_state : std_logic_vector(3 DOWNTO 0);
SIGNAL \ALT_INV_parallel_in[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_parallel_in[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_parallel_in[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_clear~input_o\ : std_logic;
SIGNAL \ALT_INV_parallel_in[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_write_number~input_o\ : std_logic;
SIGNAL \ALT_INV_enable~input_o\ : std_logic;
SIGNAL \ALT_INV_output[3]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[2]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[1]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[0]$latch~combout\ : std_logic;

BEGIN

ww_parallel_in <= parallel_in;
ww_enable <= enable;
ww_clear <= clear;
ww_clock <= clock;
ww_write_number <= write_number;
output <= ww_output;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_state[3]~1_combout\ <= NOT \state[3]~1_combout\;
ALT_INV_state(0) <= NOT state(0);
ALT_INV_state(1) <= NOT state(1);
ALT_INV_state(2) <= NOT state(2);
ALT_INV_state(3) <= NOT state(3);
\ALT_INV_parallel_in[3]~input_o\ <= NOT \parallel_in[3]~input_o\;
\ALT_INV_parallel_in[2]~input_o\ <= NOT \parallel_in[2]~input_o\;
\ALT_INV_parallel_in[1]~input_o\ <= NOT \parallel_in[1]~input_o\;
\ALT_INV_clear~input_o\ <= NOT \clear~input_o\;
\ALT_INV_parallel_in[0]~input_o\ <= NOT \parallel_in[0]~input_o\;
\ALT_INV_write_number~input_o\ <= NOT \write_number~input_o\;
\ALT_INV_enable~input_o\ <= NOT \enable~input_o\;
\ALT_INV_output[3]$latch~combout\ <= NOT \output[3]$latch~combout\;
\ALT_INV_output[2]$latch~combout\ <= NOT \output[2]$latch~combout\;
\ALT_INV_output[1]$latch~combout\ <= NOT \output[1]$latch~combout\;
\ALT_INV_output[0]$latch~combout\ <= NOT \output[0]$latch~combout\;

\output[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[0]$latch~combout\,
	devoe => ww_devoe,
	o => \output[0]~output_o\);

\output[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[1]$latch~combout\,
	devoe => ww_devoe,
	o => \output[1]~output_o\);

\output[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[2]$latch~combout\,
	devoe => ww_devoe,
	o => \output[2]~output_o\);

\output[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[3]$latch~combout\,
	devoe => ww_devoe,
	o => \output[3]~output_o\);

\clock~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clock,
	o => \clock~input_o\);

\write_number~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_write_number,
	o => \write_number~input_o\);

\parallel_in[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_parallel_in(3),
	o => \parallel_in[3]~input_o\);

\state~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \state~4_combout\ = ( \write_number~input_o\ & ( \parallel_in[3]~input_o\ & ( !state(3) $ (((!state(0)) # ((!state(1)) # (!state(2))))) ) ) ) # ( !\write_number~input_o\ & ( \parallel_in[3]~input_o\ & ( (!state(3)) # ((!state(1) & !state(2))) ) ) ) # ( 
-- \write_number~input_o\ & ( !\parallel_in[3]~input_o\ & ( !state(3) $ (((!state(0)) # ((!state(1)) # (!state(2))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000011111111011111111110000000000000111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_state(0),
	datab => ALT_INV_state(1),
	datac => ALT_INV_state(2),
	datad => ALT_INV_state(3),
	datae => \ALT_INV_write_number~input_o\,
	dataf => \ALT_INV_parallel_in[3]~input_o\,
	combout => \state~4_combout\);

\enable~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_enable,
	o => \enable~input_o\);

\clear~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clear,
	o => \clear~input_o\);

\state[3]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \state[3]~1_combout\ = (!\enable~input_o\ & !\clear~input_o\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100010001000100010001000100010001000100010001000100010001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_enable~input_o\,
	datab => \ALT_INV_clear~input_o\,
	combout => \state[3]~1_combout\);

\state[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clock~input_o\,
	d => \state~4_combout\,
	clrn => \ALT_INV_state[3]~1_combout\,
	ena => \ALT_INV_enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => state(3));

\parallel_in[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_parallel_in(2),
	o => \parallel_in[2]~input_o\);

\state~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \state~3_combout\ = ( \write_number~input_o\ & ( \parallel_in[2]~input_o\ & ( !state(2) $ (((!state(0)) # (!state(1)))) ) ) ) # ( !\write_number~input_o\ & ( \parallel_in[2]~input_o\ & ( (!state(3)) # ((!state(1) & !state(2))) ) ) ) # ( 
-- \write_number~input_o\ & ( !\parallel_in[2]~input_o\ & ( !state(2) $ (((!state(0)) # (!state(1)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000111100001111011111111110000000001111000011110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_state(0),
	datab => ALT_INV_state(1),
	datac => ALT_INV_state(2),
	datad => ALT_INV_state(3),
	datae => \ALT_INV_write_number~input_o\,
	dataf => \ALT_INV_parallel_in[2]~input_o\,
	combout => \state~3_combout\);

\state[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clock~input_o\,
	d => \state~3_combout\,
	clrn => \ALT_INV_state[3]~1_combout\,
	ena => \ALT_INV_enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => state(2));

\parallel_in[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_parallel_in(1),
	o => \parallel_in[1]~input_o\);

\state~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \state~2_combout\ = ( \write_number~input_o\ & ( \parallel_in[1]~input_o\ & ( !state(0) $ (!state(1)) ) ) ) # ( !\write_number~input_o\ & ( \parallel_in[1]~input_o\ & ( (!state(3)) # ((!state(1) & !state(2))) ) ) ) # ( \write_number~input_o\ & ( 
-- !\parallel_in[1]~input_o\ & ( !state(0) $ (!state(1)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000011001100110011011111111110000000110011001100110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_state(0),
	datab => ALT_INV_state(1),
	datac => ALT_INV_state(2),
	datad => ALT_INV_state(3),
	datae => \ALT_INV_write_number~input_o\,
	dataf => \ALT_INV_parallel_in[1]~input_o\,
	combout => \state~2_combout\);

\state[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clock~input_o\,
	d => \state~2_combout\,
	clrn => \ALT_INV_state[3]~1_combout\,
	ena => \ALT_INV_enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => state(1));

\parallel_in[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_parallel_in(0),
	o => \parallel_in[0]~input_o\);

\state~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \state~0_combout\ = ( \write_number~input_o\ & ( \parallel_in[0]~input_o\ & ( !state(0) ) ) ) # ( !\write_number~input_o\ & ( \parallel_in[0]~input_o\ & ( (!state(3)) # ((!state(1) & !state(2))) ) ) ) # ( \write_number~input_o\ & ( 
-- !\parallel_in[0]~input_o\ & ( !state(0) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101011111111110000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_state(0),
	datab => ALT_INV_state(1),
	datac => ALT_INV_state(2),
	datad => ALT_INV_state(3),
	datae => \ALT_INV_write_number~input_o\,
	dataf => \ALT_INV_parallel_in[0]~input_o\,
	combout => \state~0_combout\);

\state[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clock~input_o\,
	d => \state~0_combout\,
	clrn => \ALT_INV_state[3]~1_combout\,
	ena => \ALT_INV_enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => state(0));

\output[0]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[0]$latch~combout\ = ( \output[0]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[0]$latch~combout\ & ( !\enable~input_o\ & ( state(0) ) ) ) # ( !\output[0]$latch~combout\ & ( !\enable~input_o\ & ( state(0) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_state(0),
	datae => \ALT_INV_output[0]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[0]$latch~combout\);

\output[1]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[1]$latch~combout\ = ( \output[1]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[1]$latch~combout\ & ( !\enable~input_o\ & ( state(1) ) ) ) # ( !\output[1]$latch~combout\ & ( !\enable~input_o\ & ( state(1) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_state(1),
	datae => \ALT_INV_output[1]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[1]$latch~combout\);

\output[2]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[2]$latch~combout\ = ( \output[2]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[2]$latch~combout\ & ( !\enable~input_o\ & ( state(2) ) ) ) # ( !\output[2]$latch~combout\ & ( !\enable~input_o\ & ( state(2) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_state(2),
	datae => \ALT_INV_output[2]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[2]$latch~combout\);

\output[3]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[3]$latch~combout\ = ( \output[3]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[3]$latch~combout\ & ( !\enable~input_o\ & ( state(3) ) ) ) # ( !\output[3]$latch~combout\ & ( !\enable~input_o\ & ( state(3) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_state(3),
	datae => \ALT_INV_output[3]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[3]$latch~combout\);

ww_output(0) <= \output[0]~output_o\;

ww_output(1) <= \output[1]~output_o\;

ww_output(2) <= \output[2]~output_o\;

ww_output(3) <= \output[3]~output_o\;
END structure;


