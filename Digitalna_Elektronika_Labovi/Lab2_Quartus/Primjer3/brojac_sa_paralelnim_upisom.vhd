

-- Izvrsiti sintezu i simulaciju dekadnog brojaca sa mogucnoscu
-- paralelnog upisa

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity decade_counter is port(
	parallel_in : in STD_LOGIC_VECTOR (3 downto 0);
	enable : in STD_LOGIC;
	clear : in STD_LOGIC;
	clock : in STD_LOGIC;
	write_number : in STD_LOGIC;
	
	output : out STD_LOGIC_VECTOR (3 downto 0)
	);
end decade_counter;

architecture decade_counter of decade_counter is
signal state : STD_LOGIC_VECTOR (3 downto 0);

begin counting : process (parallel_in, enable, clear, clock, write_number)
begin

if(enable = '0') then
	if(clear = '1') then
		if(clock'event and clock = '1') then
			if(write_number = '0') then
				state <= parallel_in;
				if(state > 9) then
					state <= "0000";
				end if;
			else
				state <= state + "0001";
			end if;
		end if;
	else
		state <= "0000";
	end if;
	
	output <= state;
end if;

end process counting;
end decade_counter;
	