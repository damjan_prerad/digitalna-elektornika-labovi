-- Copyright (C) 2016  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Intel and sold by Intel or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 16.1.0 Build 196 10/24/2016 SJ Lite Edition"

-- DATE "03/20/2021 14:54:57"

-- 
-- Device: Altera 5CGXFC7C7F23C8 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	decoder_4_16 IS
    PORT (
	input : IN std_logic_vector(3 DOWNTO 0);
	enable : IN std_logic;
	output : OUT std_logic_vector(15 DOWNTO 0)
	);
END decoder_4_16;

ARCHITECTURE structure OF decoder_4_16 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_input : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_enable : std_logic;
SIGNAL ww_output : std_logic_vector(15 DOWNTO 0);
SIGNAL \output[0]~output_o\ : std_logic;
SIGNAL \output[1]~output_o\ : std_logic;
SIGNAL \output[2]~output_o\ : std_logic;
SIGNAL \output[3]~output_o\ : std_logic;
SIGNAL \output[4]~output_o\ : std_logic;
SIGNAL \output[5]~output_o\ : std_logic;
SIGNAL \output[6]~output_o\ : std_logic;
SIGNAL \output[7]~output_o\ : std_logic;
SIGNAL \output[8]~output_o\ : std_logic;
SIGNAL \output[9]~output_o\ : std_logic;
SIGNAL \output[10]~output_o\ : std_logic;
SIGNAL \output[11]~output_o\ : std_logic;
SIGNAL \output[12]~output_o\ : std_logic;
SIGNAL \output[13]~output_o\ : std_logic;
SIGNAL \output[14]~output_o\ : std_logic;
SIGNAL \output[15]~output_o\ : std_logic;
SIGNAL \input[0]~input_o\ : std_logic;
SIGNAL \input[1]~input_o\ : std_logic;
SIGNAL \input[2]~input_o\ : std_logic;
SIGNAL \input[3]~input_o\ : std_logic;
SIGNAL \Mux15~0_combout\ : std_logic;
SIGNAL \enable~input_o\ : std_logic;
SIGNAL \output[0]$latch~combout\ : std_logic;
SIGNAL \Mux15~1_combout\ : std_logic;
SIGNAL \output[1]$latch~combout\ : std_logic;
SIGNAL \Mux15~2_combout\ : std_logic;
SIGNAL \output[2]$latch~combout\ : std_logic;
SIGNAL \Mux15~3_combout\ : std_logic;
SIGNAL \output[3]$latch~combout\ : std_logic;
SIGNAL \Mux15~4_combout\ : std_logic;
SIGNAL \output[4]$latch~combout\ : std_logic;
SIGNAL \Mux15~5_combout\ : std_logic;
SIGNAL \output[5]$latch~combout\ : std_logic;
SIGNAL \Mux15~6_combout\ : std_logic;
SIGNAL \output[6]$latch~combout\ : std_logic;
SIGNAL \Mux15~7_combout\ : std_logic;
SIGNAL \output[7]$latch~combout\ : std_logic;
SIGNAL \Mux15~8_combout\ : std_logic;
SIGNAL \output[8]$latch~combout\ : std_logic;
SIGNAL \Mux15~9_combout\ : std_logic;
SIGNAL \output[9]$latch~combout\ : std_logic;
SIGNAL \Mux15~10_combout\ : std_logic;
SIGNAL \output[10]$latch~combout\ : std_logic;
SIGNAL \Mux15~11_combout\ : std_logic;
SIGNAL \output[11]$latch~combout\ : std_logic;
SIGNAL \Mux15~12_combout\ : std_logic;
SIGNAL \output[12]$latch~combout\ : std_logic;
SIGNAL \Mux15~13_combout\ : std_logic;
SIGNAL \output[13]$latch~combout\ : std_logic;
SIGNAL \Mux15~14_combout\ : std_logic;
SIGNAL \output[14]$latch~combout\ : std_logic;
SIGNAL \Mux15~15_combout\ : std_logic;
SIGNAL \output[15]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_Mux15~0_combout\ : std_logic;
SIGNAL \ALT_INV_Mux15~1_combout\ : std_logic;
SIGNAL \ALT_INV_Mux15~2_combout\ : std_logic;
SIGNAL \ALT_INV_Mux15~3_combout\ : std_logic;
SIGNAL \ALT_INV_Mux15~4_combout\ : std_logic;
SIGNAL \ALT_INV_Mux15~6_combout\ : std_logic;
SIGNAL \ALT_INV_Mux15~5_combout\ : std_logic;
SIGNAL \ALT_INV_Mux15~7_combout\ : std_logic;
SIGNAL \ALT_INV_Mux15~8_combout\ : std_logic;
SIGNAL \ALT_INV_Mux15~9_combout\ : std_logic;
SIGNAL \ALT_INV_Mux15~11_combout\ : std_logic;
SIGNAL \ALT_INV_Mux15~10_combout\ : std_logic;
SIGNAL \ALT_INV_Mux15~12_combout\ : std_logic;
SIGNAL \ALT_INV_Mux15~13_combout\ : std_logic;
SIGNAL \ALT_INV_Mux15~15_combout\ : std_logic;
SIGNAL \ALT_INV_Mux15~14_combout\ : std_logic;
SIGNAL \ALT_INV_output[0]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[1]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[2]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[3]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[4]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[5]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[6]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[7]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[8]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[9]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[11]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[10]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[12]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[13]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[14]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_output[15]$latch~combout\ : std_logic;
SIGNAL \ALT_INV_input[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_input[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_input[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_input[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_enable~input_o\ : std_logic;

BEGIN

ww_input <= input;
ww_enable <= enable;
output <= ww_output;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_Mux15~0_combout\ <= NOT \Mux15~0_combout\;
\ALT_INV_Mux15~1_combout\ <= NOT \Mux15~1_combout\;
\ALT_INV_Mux15~2_combout\ <= NOT \Mux15~2_combout\;
\ALT_INV_Mux15~3_combout\ <= NOT \Mux15~3_combout\;
\ALT_INV_Mux15~4_combout\ <= NOT \Mux15~4_combout\;
\ALT_INV_Mux15~6_combout\ <= NOT \Mux15~6_combout\;
\ALT_INV_Mux15~5_combout\ <= NOT \Mux15~5_combout\;
\ALT_INV_Mux15~7_combout\ <= NOT \Mux15~7_combout\;
\ALT_INV_Mux15~8_combout\ <= NOT \Mux15~8_combout\;
\ALT_INV_Mux15~9_combout\ <= NOT \Mux15~9_combout\;
\ALT_INV_Mux15~11_combout\ <= NOT \Mux15~11_combout\;
\ALT_INV_Mux15~10_combout\ <= NOT \Mux15~10_combout\;
\ALT_INV_Mux15~12_combout\ <= NOT \Mux15~12_combout\;
\ALT_INV_Mux15~13_combout\ <= NOT \Mux15~13_combout\;
\ALT_INV_Mux15~15_combout\ <= NOT \Mux15~15_combout\;
\ALT_INV_Mux15~14_combout\ <= NOT \Mux15~14_combout\;
\ALT_INV_output[0]$latch~combout\ <= NOT \output[0]$latch~combout\;
\ALT_INV_output[1]$latch~combout\ <= NOT \output[1]$latch~combout\;
\ALT_INV_output[2]$latch~combout\ <= NOT \output[2]$latch~combout\;
\ALT_INV_output[3]$latch~combout\ <= NOT \output[3]$latch~combout\;
\ALT_INV_output[4]$latch~combout\ <= NOT \output[4]$latch~combout\;
\ALT_INV_output[5]$latch~combout\ <= NOT \output[5]$latch~combout\;
\ALT_INV_output[6]$latch~combout\ <= NOT \output[6]$latch~combout\;
\ALT_INV_output[7]$latch~combout\ <= NOT \output[7]$latch~combout\;
\ALT_INV_output[8]$latch~combout\ <= NOT \output[8]$latch~combout\;
\ALT_INV_output[9]$latch~combout\ <= NOT \output[9]$latch~combout\;
\ALT_INV_output[11]$latch~combout\ <= NOT \output[11]$latch~combout\;
\ALT_INV_output[10]$latch~combout\ <= NOT \output[10]$latch~combout\;
\ALT_INV_output[12]$latch~combout\ <= NOT \output[12]$latch~combout\;
\ALT_INV_output[13]$latch~combout\ <= NOT \output[13]$latch~combout\;
\ALT_INV_output[14]$latch~combout\ <= NOT \output[14]$latch~combout\;
\ALT_INV_output[15]$latch~combout\ <= NOT \output[15]$latch~combout\;
\ALT_INV_input[0]~input_o\ <= NOT \input[0]~input_o\;
\ALT_INV_input[1]~input_o\ <= NOT \input[1]~input_o\;
\ALT_INV_input[2]~input_o\ <= NOT \input[2]~input_o\;
\ALT_INV_input[3]~input_o\ <= NOT \input[3]~input_o\;
\ALT_INV_enable~input_o\ <= NOT \enable~input_o\;

\output[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[0]$latch~combout\,
	devoe => ww_devoe,
	o => \output[0]~output_o\);

\output[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[1]$latch~combout\,
	devoe => ww_devoe,
	o => \output[1]~output_o\);

\output[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[2]$latch~combout\,
	devoe => ww_devoe,
	o => \output[2]~output_o\);

\output[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[3]$latch~combout\,
	devoe => ww_devoe,
	o => \output[3]~output_o\);

\output[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[4]$latch~combout\,
	devoe => ww_devoe,
	o => \output[4]~output_o\);

\output[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[5]$latch~combout\,
	devoe => ww_devoe,
	o => \output[5]~output_o\);

\output[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[6]$latch~combout\,
	devoe => ww_devoe,
	o => \output[6]~output_o\);

\output[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[7]$latch~combout\,
	devoe => ww_devoe,
	o => \output[7]~output_o\);

\output[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[8]$latch~combout\,
	devoe => ww_devoe,
	o => \output[8]~output_o\);

\output[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[9]$latch~combout\,
	devoe => ww_devoe,
	o => \output[9]~output_o\);

\output[10]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[10]$latch~combout\,
	devoe => ww_devoe,
	o => \output[10]~output_o\);

\output[11]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[11]$latch~combout\,
	devoe => ww_devoe,
	o => \output[11]~output_o\);

\output[12]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[12]$latch~combout\,
	devoe => ww_devoe,
	o => \output[12]~output_o\);

\output[13]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[13]$latch~combout\,
	devoe => ww_devoe,
	o => \output[13]~output_o\);

\output[14]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[14]$latch~combout\,
	devoe => ww_devoe,
	o => \output[14]~output_o\);

\output[15]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \output[15]$latch~combout\,
	devoe => ww_devoe,
	o => \output[15]~output_o\);

\input[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_input(0),
	o => \input[0]~input_o\);

\input[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_input(1),
	o => \input[1]~input_o\);

\input[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_input(2),
	o => \input[2]~input_o\);

\input[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_input(3),
	o => \input[3]~input_o\);

\Mux15~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~0_combout\ = (!\input[0]~input_o\ & (!\input[1]~input_o\ & (!\input[2]~input_o\ & !\input[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000100000000000000010000000000000001000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_input[0]~input_o\,
	datab => \ALT_INV_input[1]~input_o\,
	datac => \ALT_INV_input[2]~input_o\,
	datad => \ALT_INV_input[3]~input_o\,
	combout => \Mux15~0_combout\);

\enable~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_enable,
	o => \enable~input_o\);

\output[0]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[0]$latch~combout\ = ( \output[0]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[0]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~0_combout\ ) ) ) # ( !\output[0]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Mux15~0_combout\,
	datae => \ALT_INV_output[0]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[0]$latch~combout\);

\Mux15~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~1_combout\ = (\input[0]~input_o\ & (!\input[1]~input_o\ & (!\input[2]~input_o\ & !\input[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000000000000010000000000000001000000000000000100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_input[0]~input_o\,
	datab => \ALT_INV_input[1]~input_o\,
	datac => \ALT_INV_input[2]~input_o\,
	datad => \ALT_INV_input[3]~input_o\,
	combout => \Mux15~1_combout\);

\output[1]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[1]$latch~combout\ = ( \output[1]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[1]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~1_combout\ ) ) ) # ( !\output[1]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~1_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Mux15~1_combout\,
	datae => \ALT_INV_output[1]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[1]$latch~combout\);

\Mux15~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~2_combout\ = (!\input[0]~input_o\ & (\input[1]~input_o\ & (!\input[2]~input_o\ & !\input[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000000000001000000000000000100000000000000010000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_input[0]~input_o\,
	datab => \ALT_INV_input[1]~input_o\,
	datac => \ALT_INV_input[2]~input_o\,
	datad => \ALT_INV_input[3]~input_o\,
	combout => \Mux15~2_combout\);

\output[2]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[2]$latch~combout\ = ( \output[2]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[2]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~2_combout\ ) ) ) # ( !\output[2]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~2_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Mux15~2_combout\,
	datae => \ALT_INV_output[2]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[2]$latch~combout\);

\Mux15~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~3_combout\ = (\input[0]~input_o\ & (\input[1]~input_o\ & (!\input[2]~input_o\ & !\input[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000000000000100000000000000010000000000000001000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_input[0]~input_o\,
	datab => \ALT_INV_input[1]~input_o\,
	datac => \ALT_INV_input[2]~input_o\,
	datad => \ALT_INV_input[3]~input_o\,
	combout => \Mux15~3_combout\);

\output[3]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[3]$latch~combout\ = ( \output[3]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[3]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~3_combout\ ) ) ) # ( !\output[3]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~3_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Mux15~3_combout\,
	datae => \ALT_INV_output[3]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[3]$latch~combout\);

\Mux15~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~4_combout\ = (!\input[0]~input_o\ & (!\input[1]~input_o\ & (\input[2]~input_o\ & !\input[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000000000000010000000000000001000000000000000100000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_input[0]~input_o\,
	datab => \ALT_INV_input[1]~input_o\,
	datac => \ALT_INV_input[2]~input_o\,
	datad => \ALT_INV_input[3]~input_o\,
	combout => \Mux15~4_combout\);

\output[4]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[4]$latch~combout\ = ( \output[4]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[4]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~4_combout\ ) ) ) # ( !\output[4]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~4_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Mux15~4_combout\,
	datae => \ALT_INV_output[4]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[4]$latch~combout\);

\Mux15~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~5_combout\ = (\input[0]~input_o\ & (!\input[1]~input_o\ & (\input[2]~input_o\ & !\input[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000000000001000000000000000100000000000000010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_input[0]~input_o\,
	datab => \ALT_INV_input[1]~input_o\,
	datac => \ALT_INV_input[2]~input_o\,
	datad => \ALT_INV_input[3]~input_o\,
	combout => \Mux15~5_combout\);

\output[5]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[5]$latch~combout\ = ( \output[5]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[5]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~5_combout\ ) ) ) # ( !\output[5]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~5_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Mux15~5_combout\,
	datae => \ALT_INV_output[5]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[5]$latch~combout\);

\Mux15~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~6_combout\ = (!\input[0]~input_o\ & (\input[1]~input_o\ & (\input[2]~input_o\ & !\input[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001000000000000000100000000000000010000000000000001000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_input[0]~input_o\,
	datab => \ALT_INV_input[1]~input_o\,
	datac => \ALT_INV_input[2]~input_o\,
	datad => \ALT_INV_input[3]~input_o\,
	combout => \Mux15~6_combout\);

\output[6]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[6]$latch~combout\ = ( \output[6]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[6]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~6_combout\ ) ) ) # ( !\output[6]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~6_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Mux15~6_combout\,
	datae => \ALT_INV_output[6]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[6]$latch~combout\);

\Mux15~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~7_combout\ = (\input[0]~input_o\ & (\input[1]~input_o\ & (\input[2]~input_o\ & !\input[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100000000000000010000000000000001000000000000000100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_input[0]~input_o\,
	datab => \ALT_INV_input[1]~input_o\,
	datac => \ALT_INV_input[2]~input_o\,
	datad => \ALT_INV_input[3]~input_o\,
	combout => \Mux15~7_combout\);

\output[7]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[7]$latch~combout\ = ( \output[7]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[7]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~7_combout\ ) ) ) # ( !\output[7]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~7_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Mux15~7_combout\,
	datae => \ALT_INV_output[7]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[7]$latch~combout\);

\Mux15~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~8_combout\ = (!\input[0]~input_o\ & (!\input[1]~input_o\ & (!\input[2]~input_o\ & \input[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010000000000000001000000000000000100000000000000010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_input[0]~input_o\,
	datab => \ALT_INV_input[1]~input_o\,
	datac => \ALT_INV_input[2]~input_o\,
	datad => \ALT_INV_input[3]~input_o\,
	combout => \Mux15~8_combout\);

\output[8]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[8]$latch~combout\ = ( \output[8]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[8]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~8_combout\ ) ) ) # ( !\output[8]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~8_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Mux15~8_combout\,
	datae => \ALT_INV_output[8]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[8]$latch~combout\);

\Mux15~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~9_combout\ = (\input[0]~input_o\ & (!\input[1]~input_o\ & (!\input[2]~input_o\ & \input[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001000000000000000100000000000000010000000000000001000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_input[0]~input_o\,
	datab => \ALT_INV_input[1]~input_o\,
	datac => \ALT_INV_input[2]~input_o\,
	datad => \ALT_INV_input[3]~input_o\,
	combout => \Mux15~9_combout\);

\output[9]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[9]$latch~combout\ = ( \output[9]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[9]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~9_combout\ ) ) ) # ( !\output[9]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~9_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Mux15~9_combout\,
	datae => \ALT_INV_output[9]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[9]$latch~combout\);

\Mux15~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~10_combout\ = (!\input[0]~input_o\ & (\input[1]~input_o\ & (!\input[2]~input_o\ & \input[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000100000000000000010000000000000001000000000000000100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_input[0]~input_o\,
	datab => \ALT_INV_input[1]~input_o\,
	datac => \ALT_INV_input[2]~input_o\,
	datad => \ALT_INV_input[3]~input_o\,
	combout => \Mux15~10_combout\);

\output[10]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[10]$latch~combout\ = ( \output[10]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[10]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~10_combout\ ) ) ) # ( !\output[10]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~10_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Mux15~10_combout\,
	datae => \ALT_INV_output[10]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[10]$latch~combout\);

\Mux15~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~11_combout\ = (\input[0]~input_o\ & (\input[1]~input_o\ & (!\input[2]~input_o\ & \input[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010000000000000001000000000000000100000000000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_input[0]~input_o\,
	datab => \ALT_INV_input[1]~input_o\,
	datac => \ALT_INV_input[2]~input_o\,
	datad => \ALT_INV_input[3]~input_o\,
	combout => \Mux15~11_combout\);

\output[11]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[11]$latch~combout\ = ( \output[11]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[11]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~11_combout\ ) ) ) # ( !\output[11]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~11_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Mux15~11_combout\,
	datae => \ALT_INV_output[11]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[11]$latch~combout\);

\Mux15~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~12_combout\ = (!\input[0]~input_o\ & (!\input[1]~input_o\ & (\input[2]~input_o\ & \input[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001000000000000000100000000000000010000000000000001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_input[0]~input_o\,
	datab => \ALT_INV_input[1]~input_o\,
	datac => \ALT_INV_input[2]~input_o\,
	datad => \ALT_INV_input[3]~input_o\,
	combout => \Mux15~12_combout\);

\output[12]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[12]$latch~combout\ = ( \output[12]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[12]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~12_combout\ ) ) ) # ( !\output[12]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~12_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Mux15~12_combout\,
	datae => \ALT_INV_output[12]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[12]$latch~combout\);

\Mux15~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~13_combout\ = (\input[0]~input_o\ & (!\input[1]~input_o\ & (\input[2]~input_o\ & \input[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000100000000000000010000000000000001000000000000000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_input[0]~input_o\,
	datab => \ALT_INV_input[1]~input_o\,
	datac => \ALT_INV_input[2]~input_o\,
	datad => \ALT_INV_input[3]~input_o\,
	combout => \Mux15~13_combout\);

\output[13]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[13]$latch~combout\ = ( \output[13]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[13]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~13_combout\ ) ) ) # ( !\output[13]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~13_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Mux15~13_combout\,
	datae => \ALT_INV_output[13]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[13]$latch~combout\);

\Mux15~14\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~14_combout\ = (!\input[0]~input_o\ & (\input[1]~input_o\ & (\input[2]~input_o\ & \input[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000010000000000000001000000000000000100000000000000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_input[0]~input_o\,
	datab => \ALT_INV_input[1]~input_o\,
	datac => \ALT_INV_input[2]~input_o\,
	datad => \ALT_INV_input[3]~input_o\,
	combout => \Mux15~14_combout\);

\output[14]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[14]$latch~combout\ = ( \output[14]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[14]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~14_combout\ ) ) ) # ( !\output[14]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~14_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Mux15~14_combout\,
	datae => \ALT_INV_output[14]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[14]$latch~combout\);

\Mux15~15\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~15_combout\ = (\input[0]~input_o\ & (\input[1]~input_o\ & (\input[2]~input_o\ & \input[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000001000000000000000100000000000000010000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_input[0]~input_o\,
	datab => \ALT_INV_input[1]~input_o\,
	datac => \ALT_INV_input[2]~input_o\,
	datad => \ALT_INV_input[3]~input_o\,
	combout => \Mux15~15_combout\);

\output[15]$latch\ : cyclonev_lcell_comb
-- Equation(s):
-- \output[15]$latch~combout\ = ( \output[15]$latch~combout\ & ( \enable~input_o\ ) ) # ( \output[15]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~15_combout\ ) ) ) # ( !\output[15]$latch~combout\ & ( !\enable~input_o\ & ( \Mux15~15_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Mux15~15_combout\,
	datae => \ALT_INV_output[15]$latch~combout\,
	dataf => \ALT_INV_enable~input_o\,
	combout => \output[15]$latch~combout\);

ww_output(0) <= \output[0]~output_o\;

ww_output(1) <= \output[1]~output_o\;

ww_output(2) <= \output[2]~output_o\;

ww_output(3) <= \output[3]~output_o\;

ww_output(4) <= \output[4]~output_o\;

ww_output(5) <= \output[5]~output_o\;

ww_output(6) <= \output[6]~output_o\;

ww_output(7) <= \output[7]~output_o\;

ww_output(8) <= \output[8]~output_o\;

ww_output(9) <= \output[9]~output_o\;

ww_output(10) <= \output[10]~output_o\;

ww_output(11) <= \output[11]~output_o\;

ww_output(12) <= \output[12]~output_o\;

ww_output(13) <= \output[13]~output_o\;

ww_output(14) <= \output[14]~output_o\;

ww_output(15) <= \output[15]~output_o\;
END structure;


