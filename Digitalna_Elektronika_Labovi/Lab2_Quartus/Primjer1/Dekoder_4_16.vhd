

-- Napisati VHDL kod za sintezu i simulaciju dekodera 4/16
-- aktivnog na nizak nivo signala za omogucenje rada. Opisati
-- karakteristicne dijelove i prikazati rezultate simulacije

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity decoder_4_16 is port
	(
	input : in STD_LOGIC_VECTOR (3 downto 0);
	enable : in STD_LOGIC;
	output : out STD_LOGIC_VECTOR (15 downto 0)
	);
end decoder_4_16;

architecture decoder_4_16 of decoder_4_16 is
	constant in_00 : STD_LOGIC_VECTOR (3 downto 0) := "0000";
	constant in_01 : STD_LOGIC_VECTOR (3 downto 0) := "0001";
	constant in_02 : STD_LOGIC_VECTOR (3 downto 0) := "0010";
	constant in_03 : STD_LOGIC_VECTOR (3 downto 0) := "0011";
	constant in_04 : STD_LOGIC_VECTOR (3 downto 0) := "0100";
	constant in_05 : STD_LOGIC_VECTOR (3 downto 0) := "0101";
	constant in_06 : STD_LOGIC_VECTOR (3 downto 0) := "0110";
	constant in_07 : STD_LOGIC_VECTOR (3 downto 0) := "0111";
	constant in_08 : STD_LOGIC_VECTOR (3 downto 0) := "1000";
	constant in_09 : STD_LOGIC_VECTOR (3 downto 0) := "1001";
	constant in_10 : STD_LOGIC_VECTOR (3 downto 0) := "1010";
	constant in_11 : STD_LOGIC_VECTOR (3 downto 0) := "1011";
	constant in_12 : STD_LOGIC_VECTOR (3 downto 0) := "1100";
	constant in_13 : STD_LOGIC_VECTOR (3 downto 0) := "1101";
	constant in_14 : STD_LOGIC_VECTOR (3 downto 0) := "1110";
	constant in_15 : STD_LOGIC_VECTOR (3 downto 0) := "1111";
	
	constant out_00 : STD_LOGIC_VECTOR (15 downto 0) := "0000000000000001";
	constant out_01 : STD_LOGIC_VECTOR (15 downto 0) := "0000000000000010";
	constant out_02 : STD_LOGIC_VECTOR (15 downto 0) := "0000000000000100";
	constant out_03 : STD_LOGIC_VECTOR (15 downto 0) := "0000000000001000";
	constant out_04 : STD_LOGIC_VECTOR (15 downto 0) := "0000000000010000";
	constant out_05 : STD_LOGIC_VECTOR (15 downto 0) := "0000000000100000";
	constant out_06 : STD_LOGIC_VECTOR (15 downto 0) := "0000000001000000";
	constant out_07 : STD_LOGIC_VECTOR (15 downto 0) := "0000000010000000";
	constant out_08 : STD_LOGIC_VECTOR (15 downto 0) := "0000000100000000";
	constant out_09 : STD_LOGIC_VECTOR (15 downto 0) := "0000001000000000";
	constant out_10 : STD_LOGIC_VECTOR (15 downto 0) := "0000010000000000";
	constant out_11 : STD_LOGIC_VECTOR (15 downto 0) := "0000100000000000";
	constant out_12 : STD_LOGIC_VECTOR (15 downto 0) := "0001000000000000";
	constant out_13 : STD_LOGIC_VECTOR (15 downto 0) := "0010000000000000";
	constant out_14 : STD_LOGIC_VECTOR (15 downto 0) := "0100000000000000";
	constant out_15 : STD_LOGIC_VECTOR (15 downto 0) := "1000000000000000";
	
	begin decoding : process (input, enable)
	begin
	if(enable = '0') then
	case input is
		when in_00 => output <= out_00;
		when in_01 => output <= out_01;
		when in_02 => output <= out_02;
		when in_03 => output <= out_03;
		when in_04 => output <= out_04;
		when in_05 => output <= out_05;
		when in_06 => output <= out_06;
		when in_07 => output <= out_07;
		when in_08 => output <= out_08;
		when in_09 => output <= out_09;
		when in_10 => output <= out_10;
		when in_11 => output <= out_11;
		when in_12 => output <= out_12;
		when in_13 => output <= out_13;
		when in_14 => output <= out_14;
		when in_15 => output <= out_15;
	end case;
	end if;
	
	end process decoding;
	end decoder_4_16;
	
	