

-- Napisati VHDL kod za sintezu i simulaciju cetverobitnog
-- univerzalnog pomjerackog registra sa mogucnoscu paralelnog
-- upisa.

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity shift_register is port(
	enable : in STD_LOGIC;
	parallel_serial : in STD_LOGIC;
	left_right : in STD_LOGIC;
	clock : in STD_LOGIC;
	parallel_in : in STD_LOGIC_VECTOR (3 downto 0);
	serial_in : in STD_LOGIC;
	clear : in STD_LOGIC;
	
	parallel_out : out STD_LOGIC_VECTOR (3 downto 0);
	serial_out : out STD_LOGIC
	);
end shift_register;

architecture shift_register of shift_register is
signal state : STD_LOGIC_VECTOR (3 downto 0);

begin shifting : process (enable, parallel_serial, left_right, clock, parallel_in, clear, serial_in)
begin

if (enable = '0') then
	if (clear = '1') then
		if(clock'event and clock = '1') then
			if(parallel_serial = '0') then
				state <= parallel_in;
			else
				if(left_right = '0') then
					serial_out <= state(3);
					state(3) <= state(2);
					state(2) <= state(1);
					state(1) <= state(0);
					state(0) <= serial_in;
				else
					serial_out <= state(0);
					state(0) <= state(1);
					state(1) <= state(2);
					state(2) <= state(3);
					state(3) <= serial_in;
				end if;
			end if;
		end if;
	else state <= "0000";
end if;

parallel_out <= state;
end if;

end process shifting;
end shift_register;